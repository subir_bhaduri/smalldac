#////////////////////////
# DI board interface using MCP23017
# From http://raspberrypi.link-tech.de/doku.php?id=mcp23017
#////////////////////////

# Last edited: 9th Dec. 2018

# import the main functions file that refers to this module.
import global_functions_variables as gfv
import collections
import re

class DI_board(gfv.QObject):
	"""DI board class"""
	
	#counter_update = gfv.pyqtSignal(str, name='counter_update')
	DI_update = gfv.pyqtSignal(str,int, name='DI_update')
	
	def __init__(self,MainWindow,instance,parent=None):
		gfv.QObject.__init__(self, parent)
		
		self.MW = MainWindow
		self.instance = instance
	
		#-----------------------------------------------------------------------
		# On the MCP23017 side
		#-----------------------------------------------------------------------
		#State the various registor names with their addresses
		self.IODIRA			 = 0x00 # Pin direction register A
		self.IODIRB			 = 0x01 # Pin direction register B
		self.GPIOA			 = 0x12 # Register for inputs on PORTA
		self.GPIOB			 = 0x13 # Register for inputs on PORTB
		self.GPINTENA		 = 0x04 # GP interrupt for port A
		self.GPINTENB		 = 0x05 # GP interrupt for port B
		self.IOCONA			 = 0x0a # INT control for A
		self.IOCONB			 = 0x0b # INT control for port B
		self.INTFA			 = 0x0E # interrupt flag for port A
		self.INTFB			 = 0x0F # interrupt flag for port B
		self.INTCAPA		 = 0x10 # GP interrupt for port A
		self.INTCAPB		 = 0x11 # GP interrupt for port B
	
		self.df_local = gfv.sorted_df(gfv.df,'DI_board',1)
		  
		# Get general board config params.
		self.address,self.INTA_pin,self.INTB_pin = gfv.get_config_parameters('DI_board',instance,'address','INTA_pin','INTB_pin')
		#self.address = int(self.address,16)
		#print(self.address)

		# Get ports config parameters
		self.interrupt_pins, self.invert_pins, self.counters_dict  = self.get_port_types()

		# Init the board ports
		self.init_ports()
		
		self.port_names = self.df_local['variable'].values
		
		# Initiate a counters list to contain all counter current values
		self.counter_values=[0]*16
		
	def get_port_types(self):
		# Get the invert and counter features per pin
		invert_pins= []
		counters_dict = collections.defaultdict(dict)
		# Set the interrupt pins on MCP23017 as per config file.
		interrupt_pins= []
		#
		for pin_num, pin_type in enumerate(self.df_local['pin_config'].values):			
			# find if pin is interrupt pin
			if pin_type.find('interrupt')!=-1: 
				interrupt_pins.append('1')
				# find if pin is to be setup as a counter (invert necessary)
				if pin_type.find('counter')!=-1: 
					max_value = re.search("counter=(\d+)",pin_type)
					#print('Max_value:',max_value.group(1))
					counters_dict[pin_num]['max'] = int(max_value.group(1))
					# get the units/pulse value so that every time there's a pulse..
					counters_dict[pin_num]['physical_units_per_pulse'] = 1.0/int(max_value.group(1))
					# current running value of the counter. Actually redundant.
					counters_dict[pin_num]['current'] = 0
					# cycles that have passed. also represents physical units
					counters_dict[pin_num]['cycles'] = 0
			else:  
				interrupt_pins.append('0')
				
			# find if pin is inverted
			if pin_type.find('invert')!=-1: invert_pins.append('1')
			else: invert_pins.append('0')

		interrupt_pins = ''.join(interrupt_pins)
		invert_pins = ''.join(invert_pins)

		return interrupt_pins,invert_pins,counters_dict
	
	
	def init_ports(self):
		# If mode is RPi, then interact with all board functions.	 
		if gfv.mode == "RPi":
			try:
				# Set RPi pins to receive interrupts from DI board
				# set pins to pullups - to make them ready
				gfv.GPIO.setup( self.INTA_pin, gfv.GPIO.IN, pull_up_down = gfv.GPIO.PUD_UP)
				gfv.GPIO.setup( self.INTB_pin, gfv.GPIO.IN, pull_up_down = gfv.GPIO.PUD_UP)

				# Define all MCP23017 pins as inputs. 1= input, 0 = output
				gfv.bus.write_byte_data(self.address, self.IODIRA , 0xFF)	  # Define complete Port A as IN-Port
				gfv.bus.write_byte_data(self.address, self.IODIRB , 0xFF)	  # Define complete Port B as IN-Port:
				
				# Set the interrupt pins as per pin config file
				gfv.bus.write_byte_data(self.address, self.GPINTENA, int(self.interrupt_pins[:8],2))	  
				gfv.bus.write_byte_data(self.address, self.GPINTENB, int(self.interrupt_pins[8:],2))
				
				# Read interrupt registers to check if set properly
				a=gfv.bus.read_byte_data(self.address, self.GPINTENA)
				b=gfv.bus.read_byte_data(self.address, self.GPINTENB)
				
				# IOCON default is 00000000
				# (MIRROR = 1, no mirror =0)
				# since all pins are used as inputs in this case, set all as interrupts rather than polling.
				gfv.bus.write_byte_data(self.address, self.IOCONA, 0x40)
				gfv.bus.write_byte_data(self.address, self.IOCONB, 0x40)
				
				# Check if its true.
				#a='{0:08b}'.format(gfv.bus.read_byte_data(address, IOCONA))
				#b='{0:08b}'.format(gfv.bus.read_byte_data(address, IOCONB))
							
				# Read current status of the MCP23017 GPIOs to flush and reset the interrupts.
				a=gfv.bus.read_byte_data(self.address, self.GPIOA)
				b=gfv.bus.read_byte_data(self.address, self.GPIOB)
				
				# Enable interrupts on INTA_pin or B 
				# Ref: http://raspberrywebserver.com/gpio/using-interrupt-driven-gpio.html
				# Also ref: https://medium.com/@rxseger/interrupt-driven-i-o-on-raspberry-pi-3-with-leds-and-pushbuttons-rising-falling-edge-detection-36c14e640fef
				# NOTE: One may not use "bouncetime" here as it will cause the interrupts on MCP23017 to remain unusable.
				# Note: althrough the board has IntA and INTB pins, only one needs to be triggered as MIRROR is enabled.
				gfv.GPIO.add_event_detect(self.INTB_pin, gfv.GPIO.FALLING , callback= self.DI_INT_callback)
			except:
				gfv.traceback.print_exc()
				gfv.GPIO.cleanup()
				gfv.sys.exit()
		print('Initialized DI_board instance:',self.instance)
	
	#-----------------------------------------------------------------------
	# Define the callback function for an interrupt input
	#-----------------------------------------------------------------------
	def DI_INT_callback(self,channel):
		
		#Read INTF to identify which pin caused interrupt
		A_INTF='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.INTFA))
		B_INTF='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.INTFB))
		#print(A_INTF,B_INTF)
		# Reverse the lists as smbus byte is msb -> lsb i.e. ch7 -> ch0
		A_INTF = list(A_INTF)
		B_INTF = list(B_INTF)
		#print(A_INTF,B_INTF)
		A_INTF.reverse()
		B_INTF.reverse()
		#print(A_INTF,B_INTF)
		# combine the lists.
		INTF = A_INTF + B_INTF
		INTF = list(INTF)
		#Convert to integers
		INTF = [int(a) for a in INTF]
		#print('INTF: ',INTF)
		# Find first occurance of 1 in the list. If there are multiple, impling multiple inputs, then neglect.
		# to avoid errors, first check if 1 is in list.
		if 1 in INTF:
			ch_num = INTF.index(1)
			# get DI port that received the interrupt
			ch_name = self.port_names[ch_num]
			#print(ch_name)
			
			# Read all input pins, invert as necessary. Once the inputs are read, then above identification helps in flipping the right pin num.
			# reading all of GPIO or INTCAP also enables interupts 
			AB = self.get_DI()
			
			#------------------------Emit necessary pyqt signals to the main GUI----------------------
			# Check if port is an interrupt for a counter.
			if ch_num in self.counters_dict:
				# increment the current value of the counter
				self.counters_dict[ch_num]['current'] += 1
				# However, if current value > max value then zero current value. Also increment the cycles (in this case liter of water).
				if self.counters_dict[ch_num]['current'] >= self.counters_dict[ch_num]['max']:
					self.counters_dict[ch_num]['current'] = 0
				# Increment the value of water liters with decimal point
				self.counters_dict[ch_num]['cycles'] += self.counters_dict[ch_num]['physical_units_per_pulse']
			
			# Else, emit the channel name and value (1/0)
			else:
				# Else, update the Digital input with new value.
				# note that for counter pins there wont be an emit, whcih is OK and better for system response.
				self.DI_update.emit(ch_name,AB[ch_num])
	

	#-----------------------------------------------------------------------
	# Function to get DI pin status at the very instant
	#-----------------------------------------------------------------------
	def reset_counters(self):
		for pin_num in list(self.counters_dict.keys()):
			self.counters_dict[pin_num]['current'] = 0
			self.counters_dict[pin_num]['cycles'] = 0
	

		
	#-----------------------------------------------------------------------
	# Function to get DI pin status at the very instant
	#-----------------------------------------------------------------------
	def get_DI(self):
	   # Get channel value,Also doing this flushes interrupts to ready.
		A='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.GPIOA))
		B='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.GPIOB))
		#print(A+B)
		# Reverse the list as smbus byte is msb -> lsb i.e. ch7 -> ch0
		A = A[::-1]
		B = B[::-1]
		#print(A,B)
		AB = list(A+B)
		
		# convert to integers
		AB =[int(a) for a in AB]
		#print(AB)
		
		for i in range(len(AB)):
			# show counter values (cycles) instead of 1 or zer for counter pins.
			if i in list(self.counters_dict.keys()):
				AB[i] = self.counters_dict[i]['cycles']
			else:
				# invert the booleans as 0 implies ON and 1 implies OFF at the MCP23017 level.
				if AB[i] == 0: AB[i] = 1
				elif AB[i] ==1: AB[i] = 0
				
				# if Setup.xls lists the current pin_type as 'invert', the flip the values
				if self.invert_pins[i] == '1':
					if AB[i] == 0: AB[i] = 1
					elif AB[i] ==1: AB[i] = 0
		
		#print(AB)
		return(AB)
	

		 
	
