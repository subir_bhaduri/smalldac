SmallDAC - A Raspberry Pi based hardware and Raspbian based software for interfacing various sensors, datalogging and presenting a GUI.

Developed at SmallDesign, Pune (smalldesign.in).

Hardware features:
- PT100 - 8channels/board using ADS115 breakout boards
- 4-20mA / 2-10V - 8 channels/board using MCP3208 IC
- Digital Input -  16 channels/board using MCP23017 (Isolated)
- Digital Output - 16 channels/board using MCP23017 (Isolated)

Software features:
- Graphical user interface using pyQT
- User configurable display of sensor values, plots, etc.
- User configurable placement and behaviour of buttons, etc.
- Data logging
- Easy to program automation

Why all this?
- Needed low cost DAC system for clients
- For lack of better things to do in life?

Is this working in real life? (see Wiki page)
- Installed as DAC for Prof. Vishal Sardeshpande's (IITB, Mumbai) Jaggery Processing unit at Pune. Operating since Sept. 2018.
- Installed as DAC for solar steam cooking system (canteen of NCL's Venture Center, Pune). Working since October 2018.
- Developing DAC for a biomass powered vegetable dryer project (NCL's Venture Center,Pune).
- Developing DAC for a vegetable dryer profile test equipment (BARC/ICT, Mumbai) .

Current limitations/next objectives:
- Overall:
	- OK for slow systems. Currently aquires data at 5 to 10s intervals only.
- Software
	- Code is not organized properly.
	- Lacks modularity. Code is too long.
	- No efforts have been made to secure/make robust Raspberry Pi from hanging, data loss, etc.
	- Computations system could be implemented to do live data analysis on the fly or in batches. 
	- GUI uses pyQT and i wonder if there are licensing issues. If i should convert to PySide?
	- Plotting is too slow and buggy. Need better ways to do that.
	- It would be nice to make GUI with more features like animations, language support, etc.
	- Although RPi is Wifi accessible, there's no data server where anyone can visit and see the live or stored data from cloud.
- Hardware
	- Need to improve ADC resolution.
	- Need to develop univarsal analog input card for 4-20mA, PT100/thermistors and thermocouples.
	- Need robust EMC proof casing.
	- proper level convertors needed. Now using 3.3V zeners to interface RPi GPIOs, not wise!
	- Can a universal isolated DI/DO card be made?
	- Implement RS485 and RS232 communication interfaces on board
	- RTC not included onboard.